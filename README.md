## DISCONTINUED! ##

## MaterialView ##
# README #

MaterialView is a library for HTML containing a lot of [Material Design](https://www.google.com/design/spec/material-design/introduction.html) Widgets. It makes it easy to create a site that follows the google Design Guidelines.

### Version ###

This is Version 0.0.0.1 (unstable, in development)
The first number (major) increases if the API changes in such a way that your old files won't work anymore with the new version. The second newest major version is still supported; but won't receive new features.
The second number (minor) increases if the API changes a bit, allowing your old files to work with the new version.
The third number (feature) increases if a feature is added.
The last number (patch) increases if a bug get's fixed, or a feature get's changed.

If the last number is not even, the version is unstable.
If the first number is zero, the whole project is still in development.
1.* will work with 0.* files as well.

### Set up ###

See the wiki for that.

### Contribution ###

The contribution is done here on Bitbucket. You should test your code before contributing.

To contribute, you have to contact us here: [dodo2998@gmail.com](mailto:dodo2998@gmail.com)

Please give informations about your changes made and what they should do.

### Contact ###

If you want to contribute you have to write an email to us explaining what you did and what you wanted to archieve.

Or if you have questions; just ask us: [dodo2998@gmail.com](dodo2998@gmail.com)